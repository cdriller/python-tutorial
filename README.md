# Python Tutorial

In this Tutorial I want to give a introduction into Python. It is not for explaining all the Python basics and instead for giving a good feeling how you can accomplish projects with Python.

:exclamation: All the commands described are either Python specific or meant to be used with Windows and PowerShell

## Prerequisites

To follow along this Tutorial you need three things in place:

- Install Python (Download [here](https://www.python.org/downloads/))
- Install pip (comes with Python)
- Internet Connection (for installing python packages)

### Check Prerequisites

For checking **Python** installation execute following in your shell

```powershell
python --version
```

If Python is installed correctly you should see something like this (depending on the version you have installed the output changes slightly)

```powershell
Python 3.8.10
```

Same for **pip**
```powershell
# execute
pip --version

# output
pip 20.0.2 from /usr/lib/python3/dist-packages/pip (python 3.8)
```

To check weather your **Internet Connection** is properly working, try to reach this site: https://pypi.org 

If you can reach it, you are good to go!

## Set Up Your Code Editor (Visual Studio Code)

This section is only if you are using Visual Studio Code as your code editor. I highly recommend this code editor, because it is extremely flexible with many extensions and a huge community. But feel free to set up another code editor yourself.

1. Open a blank folder in Visual Studio (Call the folder for example `python-tutorial`)
1. Install the Python Extension from the extensions side pannel (execute `Ctrl + Shift + X` on windows to get to the extensions side pannel) 
> Search for python and take the extension with over 40 Million installations (usually the first one)

**You are now all set to play around with Python!**

## General Syntax in Python

You will learn most about the Python syntax while using it. But still I want to introduce two key elements about the Python syntax, which could be confusing at first.

1. tabs are relevant and not only for code styling (other Programming Languages use often curly brackets where Python uses tabs)

```python
x = 3 # will work
    x = 3 # won't work
```
2. variable type is implicitly chosen 

```python
x = 3 
x = "string" 
# it does not matter that there was first a number assigned to 'x'
# you still can assign a string.
```

> Copy the 'Python Basics' folder from this project in your own and play around with the files


## The Python Package Manager - pip

The tool to manage Python Packages is called 'pip' and is installed with Python by default. The most Python Packages are hosted at https://pypi.org 

---
### Background Information: Difference Module, Package, Library and Framework

Because those terms are often used in the context of Package Management I want to try to clarify a bit:
**Module**, **Package** and **Library** are more or less the same and just differ in the size. 

You can say:

Library include multiple Packages. Packages include multiple Modules. 

But in reality they are often used ambiguous. I would say in the context of python you usually speak about Packages. In Powershell for example you usually speak about Modules. I'm not saying there is no difference between them, but you should not worry to much about the difference for now.

A **Framework** instead is usually a Package (bigger than a Module and smaller than a Library but don't get confused if some people call it Module or Library) with predefined rules how to build on top of this Package. 

For Example: a certain naming convention for future files. You will see an example later in this Tutorial.

---



### Virtual Environment

Because you don't want to clutter your python installation with endless installed packages. You should make use of Python virtual environments. You can think of Virtual environments in Python as seperate installation of Python with its own set of installed packages.

There is a Python package for managing those virtual environments, called `virtualenv`. Install it with the following command

```powershell
pip install virtualenv
```

Afterwards execute the following command from within your project root folder.

```powershell
# from project root folder
python -m venv .venv 
```

You can now check if your virtual environment was created successfully with checking if you have a .venv folder in your project.

Until now you just have created your virtualenv and have not activated it, activate it with the folowing command

```powershell
.\.venv\Scripts\activate
```

You can deactivate it again with:
```powershell
deactivate
```

### Test Pip with pyjokes

Just to get a feeling for the usability of the Python Package Manager pip we install a little package called `pyjokes`

First activate your virtual environment again (if you have deactivated it in the previous step)

```powershell
.\.venv\Scripts\activate
```

Then install the Python package `pyjokes`

```powershell
pip install pyjokes
```

Create a new file in your project folder and copy the following content in to it (or copy the file pyjokes.py from this project in to yours)

```python
import pyjokes

print(pyjokes.get_joke())
```

Execute the file and have fun with all those jokes this package is telling you :D 

Of course nothing to special but just to give you an easy example for a Python package.

### Django (as a Example Usage of a Python Package)

We now want to use a more usefull Python package. To be more accurate we want to use a framework. The Python framework `Django`. Django is a usefull framework to build a website with Python.

First: We do not need the pyjokes package anymore. Uninstall it:

> make sure you are still in your virtualenv which you activated earlier

```powershell
pip uninstall pyjokes
```
And confirm with hitting `y` on your keyboard when prompted.

Then install Django
```powershell
pip install django
```

To check if Django was installed successfully execute 
```powershell
pip list
```
You should see `Django` in the list of all the Python packages you have installed with pip in the current environment. 

> Don't wonder for now if there are some packages in the list you don't know

```powershell
django-admin startproject django-project
```

You probably got an error! Thats for example what I meant with a framework. Theoretically you could name your project however you want but Django forces you to not use "-" in the project name. Try again with:

```powershell
django-admin startproject django_project
```

This should have worked. Now there is a folder with the name `django_project` created. Inside the folder you ave a file called `manage.py` this file is providung somd django functionalities. For example you can start the Django Server. Do so with:

```powershell
# make sure you are in the folder of the manage.py file
python manage.py runserver
```

> Don't worry about the warnings for now

You can now access the URL http://127.0.0.1:8000 in your Browser and see the Django starting page. You have successfully installed and set up Django. All this is now customizable but we will leave this for another day and call it a day for now!

Hope you have learnt a few things about Python in this little Tutorial!


## Some Ideas for the next steps

### 
I've learned Python quite well with YouTube Videos from Corey Schafer. He has a [Python Programming Beginner Tutorials](https://www.youtube.com/playlist?list=PL-osiE80TeTskrapNbzXhwoFUiLCjGgY7) Playlist and also a (bit more advanced) [Django Series](https://www.youtube.com/playlist?list=PL-osiE80TeTtoQCKZ03TU5fNfx2UY6U4p)

Those Playlists might be an helpfull starting point to improve your Python KnowLedge

### Push to remote Repository
If you already familiar with `git` try to push your project to a remote repository. Like this your Code is backed up and (more) save. Getting familiar with `git` is a extremely valuable. 
