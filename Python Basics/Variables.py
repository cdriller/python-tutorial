# Python Variables are in general quite implicit
# You don't have to be explicit if you want to store a number, a string or anything else

x = 3
print(x)
x = "Hello"
print(x)

# compared to other languages the tabs in python is not just styling, they matter!
    x = 3 # causes an error. Remove tab before the 'x' to fix

# you can also sum up multiple variables in an array (also known as list)
x = [22, "Text", 72.22]
print(x)
