

def is_even_number(number):
    """Returns True if number is even and False if not.

    Args:
        number (int): number which is checked to be even
    """
    if (number % 2) == 0 : # '%' is modulo. Modulo is calculating the rest of a division 4 % 2 is 0, 5 % 2 is 1
        print("True")
    else:
        print("False")

is_even_number(5)
