
def while_loop(limit):
    """A function which prints all numbers from 0 to the 
    passed argument limit.

    Args:
        limit (int): the highest number printed
    """
    print("while_loop Function")
    x = 0
    while not x > (limit):
        print(x)
        x += 1

def for_loop(limit):
    """A function which prints all numbers from 0 to the 
    passed argument limit.

    Args:
        limit ([int]): the highest number printed
    """
    print("for_loop Function")
    for x in range(0, (limit + 1)): # limit + 1 because the range function excludes the last number
        print(x)

def not_working_because_of_missing_tab():
"""Just a dummy function to show how tabs are relevant
"""
print("Missing tab function")


while_loop(7)
for_loop(3)

# You can also loop over an array (also known as list)
print("Loop Over Array")
array = [1, 2, 3, 5, 8, 13]
for x in array:
    print(x)
